package com.example.profile.API;

import com.example.profile.Model.BookData;
import com.example.profile.Model.BookDetails;
import com.example.profile.Model.HajiData;
import com.example.profile.Model.HajiDetail;
import com.example.profile.Model.PanduanHaji;
import com.example.profile.Model.PanduanHajiDetail;
import com.example.profile.Model.PanduanUmroh;
import com.example.profile.Model.PanduanUmrohDetail;
import com.example.profile.Model.TemplateAlq;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIInterface {
    @GET("viewalq")
    Call<BookData> getData();

    @GET("searchalq/{id}")
    Call<BookDetails> getDatadetails(@Path("id") int idSurat);

    @GET("listumroh")
    Call<PanduanUmroh> getListUmroh();

    @GET("listdataumroh/{id}")
    Call<PanduanUmrohDetail> getListUmrohDetail(@Path("id")int idDataUmroh);

    @GET("judul")
    Call<TemplateAlq> getTemplateAlq();

    @GET("listhaji")
    Call<PanduanHaji> getListHaji();

    @GET("listdatahaji/{id}")
    Call<PanduanHajiDetail> getListHajiDetail(@Path("id")int idDataHaji);
}
