package com.example.profile.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;

import com.example.profile.R;

public class Activity2 extends AppCompatActivity {

    private CardView kartu,kartu2,kartu3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        getSupportActionBar().hide();
        kartu = (CardView) findViewById(R.id.tombol);
        kartu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivity();
            }
        });

        kartu2 = (CardView) findViewById(R.id.tombol2);
        kartu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTampilanHaji();
            }
        });

        kartu3 = (CardView) findViewById(R.id.tombol3);
        kartu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTampilanUmroh();
            }
        });


    }

    public void openMainActivity(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        //finish();
    }

    public void openTampilanHaji(){

        Intent intent = new Intent(this, TampilanPanduanHaji.class);
        startActivity(intent);
        //finish();
    }

    public void openTampilanUmroh(){

        Intent intent = new Intent(this, TampilanPanduanUmroh.class);
        startActivity(intent);
        //finish();
    }




}
