package com.example.profile.Activities;



import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.profile.API.API;
import com.example.profile.API.APIInterface;
import com.example.profile.Adapter.DetailAdapter;
import com.example.profile.Model.BookDetails;
import com.example.profile.Model.Row;
import com.example.profile.Model.TemplateAlq;
import com.example.profile.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailData extends AppCompatActivity {


    List<Row> listData;
    DetailAdapter adapter;
    RecyclerView recyclerViewAlq;
    int id;
    TextView temp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.isi_surat);
        Intent intent = getIntent();
        Bundle test = intent.getBundleExtra("Paket");
        @SuppressLint("WrongViewCast") final RelativeLayout rl = findViewById(R.id.row_data_book2_relativeLayout);
        id = test.getInt(MainActivity.id_buku);
        recyclerViewAlq = findViewById(R.id.recycler_view_alq2);
        temp = (TextView) findViewById(R.id.template_arab);
        //actionBar.setTitle(Html.fromHtml("<font color=\"red\">" + getString(R.string.app_name) + "</font>"));
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000\">" + test.getString(MainActivity.nama_surah) + "</font>"));
        getData(id);
        getData2();
    }


    private void getData(int id) {
        APIInterface apiInterface = API.getClient().create(APIInterface.class);
        Call<BookDetails> call = apiInterface.getDatadetails(id);
        call.enqueue(new Callback<BookDetails>() {
            @Override
            public void onResponse(Call<BookDetails> call, Response<BookDetails> response) {
                listData = response.body().getRows();
                adapter = new DetailAdapter(listData);
                recyclerViewAlq.setLayoutManager(new LinearLayoutManager(DetailData.this));
                recyclerViewAlq.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<BookDetails> call, Throwable t) {
                Log.d("ERROR!", t.getMessage());
            }
        });


        }

    private void getData2 () {
        APIInterface apiInterface = API.getClient().create(APIInterface.class);
        Call<TemplateAlq> call = apiInterface.getTemplateAlq();
        call.enqueue(new Callback<TemplateAlq>() {
            @Override
            public void onResponse(Call<TemplateAlq> call, Response<TemplateAlq> response) {
                listData = response.body().getRows();
                temp.setText(listData.get(0).getArabTemp());


            }

            @Override
            public void onFailure(Call<TemplateAlq> call, Throwable t) {
                Log.d("ERROR!", t.getMessage());
            }
        });
    }
    }

