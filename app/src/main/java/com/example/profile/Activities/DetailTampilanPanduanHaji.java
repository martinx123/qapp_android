package com.example.profile.Activities;



import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.profile.API.API;
import com.example.profile.API.APIInterface;
import com.example.profile.Adapter.AdapterPanduanHajiDetail;
import com.example.profile.Adapter.AdapterPanduanUmrohDetail;
import com.example.profile.Adapter.DetailAdapter;
import com.example.profile.Model.PanduanHajiDetail;
import com.example.profile.Model.PanduanUmrohDetail;
import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailTampilanPanduanHaji extends AppCompatActivity {


    List<Row> listData;
    AdapterPanduanHajiDetail adapter;
    RecyclerView recyclerViewHaji;
    int id;
    ImageView btnplay;

    @Override
    protected void onCreate( Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_panduan_haji);
        Intent intent = getIntent();
        Bundle test = intent.getBundleExtra("Paket3");
        @SuppressLint("WrongViewCast") final RelativeLayout rl = findViewById(R.id.row_data_book8_relativeLayout);
        id = test.getInt(TampilanPanduanHaji.id_haji);
        recyclerViewHaji = findViewById(R.id.recycler_view_detail_tampilan_haji);

        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000\">" + test.getString(TampilanPanduanHaji.list_haji) + "</font>"));
        getData(id);

        final MediaPlayer notif = MediaPlayer.create(DetailTampilanPanduanHaji.this, R.raw.consequence);
        btnplay = findViewById(R.id.btnplay2);
        btnplay.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(notif.isPlaying()){
                    notif.pause();
                    btnplay.setBackgroundResource(R.drawable.playbuttoncircled);
                }
                else{
                    notif.start();
                    btnplay.setBackgroundResource(R.drawable.stopcircled);
                }
            }
        });
    }


    private void getData(int id) {
        APIInterface apiInterface = API.getClient().create(APIInterface.class);
        Call<PanduanHajiDetail> call = apiInterface.getListHajiDetail(id);
        call.enqueue(new Callback<PanduanHajiDetail>() {
            @Override
            public void onResponse(Call<PanduanHajiDetail> call, Response<PanduanHajiDetail> response) {
                listData = response.body().getRows();
                adapter = new AdapterPanduanHajiDetail(listData);
                recyclerViewHaji.setLayoutManager(new LinearLayoutManager(DetailTampilanPanduanHaji.this));
                recyclerViewHaji.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<PanduanHajiDetail> call, Throwable t) {
                Log.d("ERROR!", t.getMessage());
            }
        });
    }
}
