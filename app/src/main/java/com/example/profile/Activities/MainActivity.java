package com.example.profile.Activities;



import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.support.v7.widget.SearchView;

import com.example.profile.API.API;
import com.example.profile.API.APIInterface;
import com.example.profile.Adapter.AdapterBookView;
import com.example.profile.Helper.ItemClickView;
import com.example.profile.Model.BookData;
import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static final String id_buku = "id_buku";
    public static final String nama_surah = "nama_surah";

    List<Row> listData;
    AdapterBookView adapter;
    RecyclerView recyclerViewAlq;


    @Override
    protected void onCreate( Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerViewAlq = findViewById(R.id.recycler_view_alq);
        @SuppressLint("WrongViewCast") final RelativeLayout rl = findViewById(R.id.row_data_book_relativeLayout);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(Html.fromHtml("<font color=\"#000\">" + "Al Qur'an" + "</font>"));
        //actionBar.setTitle(Html.fromHtml("<font color=\"red\">" + getString(R.string.app_name) + "</font>"));

        ItemClickView.addTo(recyclerViewAlq).setOnItemClickListener(new ItemClickView.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent=new Intent(MainActivity.this, DetailData.class);
                Bundle bundle = new Bundle();
                bundle.putInt(id_buku,listData.get(position).getId());
                bundle.putString("nama_isi",listData.get(position).getArti());
                bundle.putString(nama_surah,listData.get(position).getSurahName());
                intent.putExtra("Paket",bundle);

                startActivity(intent);
            }
        });

        getData();
    }

    private void getData() {
        APIInterface apiInterface = API.getClient().create(APIInterface.class);
        Call<BookData> call = apiInterface.getData();
        call.enqueue(new Callback<BookData>() {
            @Override
            public void onResponse(Call<BookData> call, Response<BookData> response) {
                listData = response.body().getRows();
                adapter = new AdapterBookView(listData);
                recyclerViewAlq.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                recyclerViewAlq.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<BookData> call, Throwable t) {
                Log.d("ERROR!", t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu,menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }
}
