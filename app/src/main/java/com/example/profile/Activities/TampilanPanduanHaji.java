package com.example.profile.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.profile.API.API;
import com.example.profile.API.APIInterface;
import com.example.profile.Adapter.AdapterBookView;
import com.example.profile.Adapter.AdapterPanduanHaji;
import com.example.profile.Adapter.AdapterPanduanUmroh;
import com.example.profile.Helper.ItemClickView;
import com.example.profile.Model.PanduanHaji;
import com.example.profile.Model.PanduanHajiDetail;
import com.example.profile.Model.PanduanUmroh;
import com.example.profile.Model.PanduanUmroh;
import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TampilanPanduanHaji extends AppCompatActivity {

    public static final String id_haji = "id_haji";
    public static final String list_haji = "list_haji";
    Button btnplay;
    List<Row> listData;
    AdapterPanduanHaji adapter;
    RecyclerView recyclerViewHaji;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilan_haji);

        recyclerViewHaji = findViewById(R.id.recycler_view_haji);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000\">" + "Panduan Haji" + "</font>"));
        final MediaPlayer notif = MediaPlayer.create(TampilanPanduanHaji.this, R.raw.consequence);
        btnplay = findViewById(R.id.btnplay);

//        btnplay.setOnClickListener(new View.OnClickListener(){
//            @Override

//            public void onClick(View v) {
//
//                if(notif.isPlaying()){
//                    notif.pause();
//                    btnplay.setBackgroundResource(R.drawable.playbuttoncircled);
//                }
//                else{
//                    notif.start();
//                    btnplay.setBackgroundResource(R.drawable.stopcircled);
//                }
//            }
//        });

        ItemClickView.addTo(recyclerViewHaji).setOnItemClickListener(new ItemClickView.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent=new Intent(TampilanPanduanHaji.this, DetailTampilanPanduanHaji.class);
                Bundle bundle = new Bundle();

                bundle.putString(list_haji,listData.get(position).getListHaji());
                bundle.putInt(id_haji,listData.get(position).getIdHaji());
                intent.putExtra("Paket3",bundle);

                startActivity(intent);
            }
        });

        getData();
    }

    private void getData() {
        APIInterface apiInterface = API.getClient().create(APIInterface.class);
        Call<PanduanHaji> call = apiInterface.getListHaji();
        call.enqueue(new Callback<PanduanHaji>() {
            @Override
            public void onResponse(Call<PanduanHaji> call, Response<PanduanHaji> response) {
                listData = response.body().getRows();
                adapter = new AdapterPanduanHaji(listData);
                recyclerViewHaji.setLayoutManager(new LinearLayoutManager(TampilanPanduanHaji.this));
                recyclerViewHaji.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<PanduanHaji> call, Throwable t) {
                Log.d("ERROR!", t.getMessage());
            }
        });
    }


}









