package com.example.profile.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.profile.API.API;
import com.example.profile.API.APIInterface;
import com.example.profile.Adapter.AdapterBookView;
import com.example.profile.Adapter.AdapterPanduanUmroh;
import com.example.profile.Helper.ItemClickView;
import com.example.profile.Model.PanduanUmroh;
import com.example.profile.Model.PanduanUmroh;
import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TampilanPanduanUmroh extends AppCompatActivity {

    public static final String id_umroh = "id_umroh";
    public static final String list_umroh = "list_umroh";
    Button btnplay;
    List<Row> listData;
    AdapterPanduanUmroh adapter;
    RecyclerView recyclerViewUmroh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilan_data_umroh1);

        recyclerViewUmroh = findViewById(R.id.recycler_view_list_umroh);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#000\">" + "Panduan Umroh" + "</font>"));
        final MediaPlayer notif = MediaPlayer.create(TampilanPanduanUmroh.this, R.raw.consequence);
        btnplay = findViewById(R.id.btnplay);

//        btnplay.setOnClickListener(new View.OnClickListener(){
//            @Override

//            public void onClick(View v) {
//
//                if(notif.isPlaying()){
//                    notif.pause();
//                    btnplay.setBackgroundResource(R.drawable.playbuttoncircled);
//                }
//                else{
//                    notif.start();
//                    btnplay.setBackgroundResource(R.drawable.stopcircled);
//                }
//            }
//        });

        ItemClickView.addTo(recyclerViewUmroh).setOnItemClickListener(new ItemClickView.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent=new Intent(TampilanPanduanUmroh.this, DetailTampilanPanduanUmroh.class);
                Bundle bundle = new Bundle();

                bundle.putString(list_umroh,listData.get(position).getListUmroh());
                bundle.putInt(id_umroh,listData.get(position).getIdUmroh());
                intent.putExtra("Paket3",bundle);

                startActivity(intent);
            }
        });

        getData();
    }

    private void getData() {
        APIInterface apiInterface = API.getClient().create(APIInterface.class);
        Call<PanduanUmroh> call = apiInterface.getListUmroh();
        call.enqueue(new Callback<PanduanUmroh>() {
            @Override
            public void onResponse(Call<PanduanUmroh> call, Response<PanduanUmroh> response) {
                listData = response.body().getRows();
                adapter = new AdapterPanduanUmroh(listData);
                recyclerViewUmroh.setLayoutManager(new LinearLayoutManager(TampilanPanduanUmroh.this));
                recyclerViewUmroh.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<PanduanUmroh> call, Throwable t) {
                Log.d("ERROR!", t.getMessage());
            }
        });
    }


        }









