package com.example.profile.Adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterBookView extends RecyclerView.Adapter<AdapterBookView.ListViewHolder> implements Filterable {
    List<Row> mListBook;
    List<Row> mListBook2;

    public AdapterBookView(List<Row> mListBook) {
        this.mListBook = mListBook;
        mListBook2 = new ArrayList<>(mListBook);
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_data_book, viewGroup, false);
        return new ListViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
       // listViewHolder.nomor.setText(String.valueOf( mListBook.get(i).getId()));
        listViewHolder.namaSurah.setText(mListBook.get(i).getSurahName());
       // listViewHolder.bahasa_arab.setText(mListBook.get(i).getBahasaArab());

    }

    @Override
    public int getItemCount() {
        return mListBook.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView namaSurah, bahasa_arab,nomor;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
          //  nomor = itemView.findViewById(R.id.nomor);
            namaSurah = itemView.findViewById(R.id.nama_buku);
           // bahasa_arab = itemView.findViewById(R.id.bahasa_arab);
        }
    }

    @Override
    public Filter getFilter() {
        return listFilter;
    }

    private Filter listFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Row> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0){
                filteredList.addAll(mListBook2);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Row item : mListBook2){
                    if(item.getSurahName().toLowerCase().contains(filterPattern)){
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mListBook.clear();
            mListBook.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
