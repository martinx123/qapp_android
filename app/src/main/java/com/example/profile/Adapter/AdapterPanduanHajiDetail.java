package com.example.profile.Adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

public class AdapterPanduanHajiDetail extends RecyclerView.Adapter<AdapterPanduanHajiDetail.ListViewHolder> {

    List<Row> mListBook;

    public AdapterPanduanHajiDetail(List<Row> mListBook) {
        this.mListBook = mListBook;
    }

    @NonNull
    @Override
    public AdapterPanduanHajiDetail.ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_data_book_8, viewGroup, false);
        return new AdapterPanduanHajiDetail.ListViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPanduanHajiDetail.ListViewHolder listViewHolder, int i) {
        listViewHolder.latin.setText(String.valueOf( mListBook.get(i).getDataHajiLatin()));
        listViewHolder.arti.setText(mListBook.get(i).getDataHajiArti());
        listViewHolder.arab.setText(mListBook.get(i).getDataHajiArab());
    }

    @Override
    public int getItemCount() {
        return mListBook.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView arti, arab,latin;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            arti = itemView.findViewById(R.id.bahasa_indo_haji);
            arab = itemView.findViewById(R.id.bahasa_arab_haji);
            latin = itemView.findViewById(R.id.bahasa_latin_haji);
        }
    }

}

