package com.example.profile.Adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

public class AdapterPanduanUmroh extends RecyclerView.Adapter<AdapterPanduanUmroh.ListViewHolder> {

    List<Row> mListBook;

    public AdapterPanduanUmroh(List<Row> mListBook) {
        this.mListBook = mListBook;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_data_book_5, viewGroup, false);
        return new ListViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
       // listViewHolder.id_list.setText(String.valueOf( mListBook.get(i).getIdList()));
        listViewHolder.list_umroh.setText(mListBook.get(i).getListUmroh());



    }

    @Override
    public int getItemCount() {
        return mListBook.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView list_umroh;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);


            list_umroh = itemView.findViewById(R.id.list_umroh);

        }
    }

}

