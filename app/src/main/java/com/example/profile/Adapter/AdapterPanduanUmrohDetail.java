package com.example.profile.Adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

public class AdapterPanduanUmrohDetail extends RecyclerView.Adapter<AdapterPanduanUmrohDetail.ListViewHolder> {

    List<Row> mListBook;

    public AdapterPanduanUmrohDetail(List<Row> mListBook) {
        this.mListBook = mListBook;
    }

    @NonNull
    @Override
    public AdapterPanduanUmrohDetail.ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_data_book_6, viewGroup, false);
        return new AdapterPanduanUmrohDetail.ListViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPanduanUmrohDetail.ListViewHolder listViewHolder, int i) {
        listViewHolder.latin.setText(String.valueOf( mListBook.get(i).getListDataUmrohLatin()));
        listViewHolder.arti.setText(mListBook.get(i).getListDataUmrohArti());
        listViewHolder.arab.setText(mListBook.get(i).getListDataUmrohArab());
    }

    @Override
    public int getItemCount() {
        return mListBook.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView arti, arab,latin;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            arti = itemView.findViewById(R.id.bahasa_indo_umroh);
            arab = itemView.findViewById(R.id.bahasa_arab_umroh);
            latin = itemView.findViewById(R.id.bahasa_latin_umroh);
        }
    }

}

