package com.example.profile.Adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.profile.Model.Row;
import com.example.profile.R;

import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ListViewHolder> {

    List<Row> mListBook;

    public DetailAdapter(List<Row> mListBook) {
        this.mListBook = mListBook;
    }

    @NonNull
    @Override
    public DetailAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_data_book2, viewGroup, false);
        return new DetailAdapter.ListViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull DetailAdapter.ListViewHolder listViewHolder, int i) {
        listViewHolder.nomor_ayat.setText(String.valueOf( mListBook.get(i).getAyat()));
        listViewHolder.arti.setText(mListBook.get(i).getArti());
        listViewHolder.arab.setText(mListBook.get(i).getArab());

    }

    @Override
    public int getItemCount() {
        return mListBook.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView arti, arab,nomor_ayat,temp;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            arti = itemView.findViewById(R.id.nama_buku2);
            arab = itemView.findViewById(R.id.bahasa_arab2);
            nomor_ayat = itemView.findViewById(R.id.nomor_ayat);
            temp = itemView.findViewById(R.id.template_arab);
        }
    }

}

