package com.example.profile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Field {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tableID")
    @Expose
    private Integer tableID;
    @SerializedName("columnID")
    @Expose
    private Integer columnID;
    @SerializedName("dataTypeID")
    @Expose
    private Integer dataTypeID;
    @SerializedName("dataTypeSize")
    @Expose
    private Integer dataTypeSize;
    @SerializedName("dataTypeModifier")
    @Expose
    private Integer dataTypeModifier;
    @SerializedName("format")
    @Expose
    private String format;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTableID() {
        return tableID;
    }

    public void setTableID(Integer tableID) {
        this.tableID = tableID;
    }

    public Integer getColumnID() {
        return columnID;
    }

    public void setColumnID(Integer columnID) {
        this.columnID = columnID;
    }

    public Integer getDataTypeID() {
        return dataTypeID;
    }

    public void setDataTypeID(Integer dataTypeID) {
        this.dataTypeID = dataTypeID;
    }

    public Integer getDataTypeSize() {
        return dataTypeSize;
    }

    public void setDataTypeSize(Integer dataTypeSize) {
        this.dataTypeSize = dataTypeSize;
    }

    public Integer getDataTypeModifier() {
        return dataTypeModifier;
    }

    public void setDataTypeModifier(Integer dataTypeModifier) {
        this.dataTypeModifier = dataTypeModifier;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

}