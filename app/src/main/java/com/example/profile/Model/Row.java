package com.example.profile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Row {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("surah_name")
    @Expose
    private String surahName;
    @SerializedName("bahasa_arab")
    @Expose
    private String bahasaArab;


    @SerializedName("ayat")
    @Expose
    private Integer ayat;
    @SerializedName("arti")
    @Expose
    private String arti;
    @SerializedName("arab")
    @Expose
    private String arab;

    @SerializedName("id_list")
    @Expose
    private Integer idList;




    @SerializedName("detail_panduan")
    @Expose
    private String detailPanduan;

    @SerializedName("id_umroh")
    @Expose
    private Integer idUmroh;
    @SerializedName("list_umroh")
    @Expose
    private String listUmroh;

    @SerializedName("id_data_umroh")
    @Expose
    private Integer idDataUmroh;

    @SerializedName("list_data_umroh_arab")
    @Expose
    private String listDataUmrohArab;
    @SerializedName("list_data_umroh_latin")
    @Expose
    private String listDataUmrohLatin;
    @SerializedName("list_data_umroh_arti")
    @Expose
    private String listDataUmrohArti;


    @SerializedName("arab_temp")
    @Expose
    private String arabTemp;

    @SerializedName("id_haji")
    @Expose
    private Integer idHaji;
    @SerializedName("list_haji")
    @Expose
    private String listHaji;

    @SerializedName("id_data_haji")
    @Expose
    private Integer idDataHaji;

    @SerializedName("data_haji_arab")
    @Expose
    private String dataHajiArab;
    @SerializedName("data_haji_latin")
    @Expose
    private String dataHajiLatin;

    @SerializedName("data_haji_arti")
    @Expose
    private String dataHajiArti;

    public String getDataHajiArti() {
        return dataHajiArti;
    }

    public void setDataHajiArti(String dataHajiArti) {
        this.dataHajiArti = dataHajiArti;
    }


    public Integer getIdDataHaji() {
        return idDataHaji;
    }

    public void setIdDataHaji(Integer idDataHaji) {
        this.idDataHaji = idDataHaji;
    }


    public String getDataHajiArab() {
        return dataHajiArab;
    }

    public void setDataHajiArab(String dataHajiArab) {
        this.dataHajiArab = dataHajiArab;
    }

    public String getDataHajiLatin() {
        return dataHajiLatin;
    }

    public void setDataHajiLatin(String dataHajiLatin) {
        this.dataHajiLatin = dataHajiLatin;
    }


    public Integer getIdHaji() {
        return idHaji;
    }

    public void setIdHaji(Integer idHaji) {
        this.idHaji = idHaji;
    }

    public String getListHaji() {
        return listHaji;
    }

    public void setListHaji(String listHaji) {
        this.listHaji = listHaji;
    }

    public String getArabTemp() {
        return arabTemp;
    }

    public void setArabTemp(String arabTemp) {
        this.arabTemp = arabTemp;
    }
    public Integer getIdDataUmroh() {
        return idDataUmroh;
    }

    public void setIdDataUmroh(Integer idDataUmroh) {
        this.idDataUmroh = idDataUmroh;
    }



    public String getListDataUmrohArab() {
        return listDataUmrohArab;
    }

    public void setListDataUmrohArab(String listDataUmrohArab) {
        this.listDataUmrohArab = listDataUmrohArab;
    }

    public String getListDataUmrohLatin() {
        return listDataUmrohLatin;
    }

    public void setListDataUmrohLatin(String listDataUmrohLatin) {
        this.listDataUmrohLatin = listDataUmrohLatin;
    }

    public String getListDataUmrohArti() {
        return listDataUmrohArti;
    }

    public void setListDataUmrohArti(String listDataUmrohArti) {
        this.listDataUmrohArti = listDataUmrohArti;
    }

    //-----------------------------------------------------------------------

    public Integer getIdUmroh() {
        return idUmroh;
    }

    public void setIdUmroh(Integer idUmroh) {
        this.idUmroh = idUmroh;
    }

    public String getListUmroh() {
        return listUmroh;
    }

    public void setListUmroh(String listUmroh) {
        this.listUmroh = listUmroh;
    }


    public String getDetailPanduan() {
        return detailPanduan;
    }

    public void setDetailPanduan(String detailPanduan) {
        this.detailPanduan = detailPanduan;
    }


    public Integer getIdList() {
        return idList;
    }

    public void setIdList(Integer idList) {
        this.idList = idList;
    }



    public Integer getAyat() {
        return ayat;
    }

    public void setAyat(Integer ayat) {
        this.ayat = ayat;
    }

    public String getArti() {
        return arti;
    }

    public void setArti(String arti) {
        this.arti = arti;
    }

    public String getArab() {
        return arab;
    }

    public void setArab(String arab) {
        this.arab = arab;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSurahName() {
        return surahName;
    }

    public void setSurahName(String surahName) {
        this.surahName = surahName;
    }

    public String getBahasaArab() {
        return bahasaArab;
    }

    public void setBahasaArab(String bahasaArab) {
        this.bahasaArab = bahasaArab;
    }

}