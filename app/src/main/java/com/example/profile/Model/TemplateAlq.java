package com.example.profile.Model;

import java.sql.Types;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TemplateAlq {

    @SerializedName("command")
    @Expose
    private String command;
    @SerializedName("rowCount")
    @Expose
    private Integer rowCount;
    @SerializedName("oid")
    @Expose
    private Object oid;
    @SerializedName("rows")
    @Expose
    private List<Row> rows = null;
    @SerializedName("fields")
    @Expose
    private List<Field> fields = null;
    @SerializedName("_parsers")
    @Expose
    private List<Object> parsers = null;
    @SerializedName("_types")
    @Expose
    private Types types;
    @SerializedName("RowCtor")
    @Expose
    private Object rowCtor;
    @SerializedName("rowAsArray")
    @Expose
    private Boolean rowAsArray;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public Object getOid() {
        return oid;
    }

    public void setOid(Object oid) {
        this.oid = oid;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public List<Object> getParsers() {
        return parsers;
    }

    public void setParsers(List<Object> parsers) {
        this.parsers = parsers;
    }

    public Types getTypes() {
        return types;
    }

    public void setTypes(Types types) {
        this.types = types;
    }

    public Object getRowCtor() {
        return rowCtor;
    }

    public void setRowCtor(Object rowCtor) {
        this.rowCtor = rowCtor;
    }

    public Boolean getRowAsArray() {
        return rowAsArray;
    }

    public void setRowAsArray(Boolean rowAsArray) {
        this.rowAsArray = rowAsArray;
    }

}